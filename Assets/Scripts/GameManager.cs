﻿using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Pun;
using Photon.Realtime;


namespace Com.IglooVision.Showroom
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        #region Public fields
        [Tooltip("The prefab that will be created to represent the player. Must be within a Resources folder")]
        public GameObject playerPrefab;

        [Tooltip("Spawn location transform for the player prefab")]
        public Transform[] spawnTransform;

        #endregion

        #region Mono CallBacks

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity once the scene is loaded. 
        /// </summary>
        private void Start()
        {
            if(playerPrefab == null || spawnTransform == null)
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
            }
            else
            {
                Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManager.GetActiveScene().name);
                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                PhotonNetwork.Instantiate(this.playerPrefab.name, spawnTransform[PhotonNetwork.LocalPlayer.ActorNumber].position, Quaternion.identity, 0);
            }
        }
        #endregion

        #region Photon Callbacks

        /// <summary>
        /// Called when the local player has left the room
        /// Launches the start menu scene for them. 
        /// </summary>
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.LogFormat("OnPlayerEnteredRoom() {0}", newPlayer.NickName); // not seen if you're the player connecting

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
            }
        }

        public override void OnPlayerLeftRoom(Player other)
        {
            Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlayerLeftRom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
            }
        }    

        #endregion

        #region Public Methods

        /// <summary>
        /// Accessible function for the player to be able to leave the room.
        /// </summary>
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        #endregion

        #region Private Methods

        //void LoadLevel()
        //{
        //    if (!PhotonNetwork.IsMasterClient)
        //    {
        //        Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
        //        return;
        //    }
        //    Debug.LogFormat("PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);
        //    PhotonNetwork.LoadLevel(1);
        //}

        #endregion
    }
}
