﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace Com.IglooVision.Showroom
{
    public class IglooPlayerUI : MonoBehaviour
    {
        #region Public Fields
        [Tooltip("UI Text to display Player's Name")]
        [SerializeField]
        private Text playerNameText;

        private IglooFirstPersonController target; // Player for this UI.

        #endregion

        #region Private Fields

        float characterControllerHeight = 0f;
        Transform targetTransform;
        Renderer targetRenderer;
        CanvasGroup _canvasGroup;
        Vector3 targetPosition;

        #endregion


        #region MonoBehaviour Callbacks

        void Awake()
        {
            _canvasGroup = this.GetComponent<CanvasGroup>();
        }

        void Update()
        {
            // Destroy itself if the target is null, It's a fail safe when Photon is destroying Instances of a Player over the network
            if (target == null)
            {
                Destroy(this.gameObject);
                return;
            }
        }

        void LateUpdate()
        {
            // Do not show the UI if we are not visible to the camera, thus avoid potential bugs with seeing the UI, but not the player itself.
            if (targetRenderer != null)
            {
                this._canvasGroup.alpha = targetRenderer.isVisible ? 1f : 0f;
            }
        }

        #endregion


        #region Public Methods

        public void SetTarget(IglooFirstPersonController _target)
        {
            // Cache references for efficiency
            target = _target;
            CharacterController characterController = _target.GetComponent<CharacterController>();
            targetTransform = this.target.GetComponent<Transform>();
            this.transform.SetParent(targetTransform);
            this.transform.position = new Vector3(0, characterController.height + 0.1f, 0);
            this.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            this.transform.localRotation = Quaternion.identity;

            targetRenderer = this.target.GetComponent<Renderer>();
           
            if (playerNameText != null && target.GetComponent<PhotonView>().Controller != null)
            {
                playerNameText.text = target.GetComponent<PhotonView>().Controller.UserId;
            }
        }

        #endregion
    }
}
