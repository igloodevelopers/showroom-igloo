﻿using UnityEngine.UI;

using Photon.Realtime;
using Photon.Pun;


#pragma warning disable 618, 649
namespace Com.IglooVision.Showroom
{
    public class PlayerPanelManager : MonoBehaviourPunCallbacks
    {
        #region UNITY

        public Text playersTextbox;
        private string playerList = "";

        public void Start()
        {
            UpdatePlayerList();
        }

        private void UpdatePlayerList()
        {
            if (!playersTextbox) return;
            playerList = "";
            foreach (Player p in PhotonNetwork.PlayerList)
            {
                string extra = "";
                if (p.IsMasterClient) extra = "M: ";
                playerList = playerList + $"{extra}{p.UserId} \n";
            }
            playersTextbox.text = playerList;
        }

        #endregion

        #region PUN CALLBACKS

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            UpdatePlayerList();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            UpdatePlayerList();
        }

        #endregion
    }
}
