﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace Com.IglooVision.Showroom
{
    [RequireComponent(typeof(BoxCollider))]
    public class Selection_System : MonoBehaviour
    {
        [Header("Button selection adjustments")]
        public Sprite progressSprite;
        public Sprite downloadSprite;
        public Sprite openSprite;

        [HideInInspector]
        public bool thisIglooIsSelected
        {
            get { return _thisIglooIsSelected; }
            set
            {
                _thisIglooIsSelected = value;
                if (_thisIglooIsSelected == true)
                {
                    btn.image.sprite = openSprite;
                    btn.image.fillAmount = 1;
                }
                else
                {
                    btn.image.sprite = downloadSprite;
                    btn.image.fillAmount = 1;
                }
            }
        }
        private bool _thisIglooIsSelected;
        private float openWaitTime = 3.0f;
        private IglooController IGcontroller;
        internal Button btn;
        private Text iglooText = null;
        private bool isHit = false;

        private void Awake()
        {
            gameObject.layer = LayerMask.NameToLayer("damagearea");
            btn = GetComponentInChildren<Button>();
        }

        void LoadIgloo(int iglooIndex)
        {
            if (!IGcontroller) IGcontroller = FindObjectOfType<IglooController>();
            IGcontroller.LoadIgloo(iglooIndex);
        }

        public void NoLongerHit()
        {
            isHit = false;
            StopAllCoroutines();

            if (!_thisIglooIsSelected)
            {
                btn.image.sprite = downloadSprite;
                btn.image.fillAmount = 1;
            }
            else
            {
                btn.image.sprite = openSprite;
                btn.image.fillAmount = 1;
            }
        }


        public void HitByLaser()
        {
            if (!isHit)
            {
                StartCoroutine(WaitForLoad());
                isHit = true;
            }
        }

        IEnumerator WaitForLoad()
        {
            float curTime = 0.0f;
            btn.image.fillAmount = 0;
            btn.image.sprite = progressSprite;
            while (curTime < openWaitTime)
            {
                btn.image.fillAmount = curTime / openWaitTime;
                curTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            btn.onClick.Invoke();
            btn.image.fillAmount = 1;
            btn.image.sprite = openSprite;
            Debug.Log("CHANGING SCENE!");
        }

#if UNITY_EDITOR
        void OnDrawGizmosSelected()
        {
            var c = GetComponent<BoxCollider>();
            Gizmos.matrix = transform.localToWorldMatrix;
            if (gameObject == UnityEditor.Selection.activeGameObject)
            {
                // If we are directly selected (and not just our parent is selected)
                // draw with negative size to get an 'inside out' cube we can see from the inside
                Gizmos.color = new Color(1.0f, 1.0f, 0.5f, 0.8f);
                Gizmos.DrawCube(c.center, -c.size);
            }
            Gizmos.color = new Color(1.0f, 0.5f, 0.5f, 0.3f);
            Gizmos.DrawCube(c.center, c.size);
        }
#endif
    }
}
