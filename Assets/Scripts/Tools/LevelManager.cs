﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Com.IglooVision.Showroom
{
    public enum LevelState
    {
        Loading,
        Loaded,
    }

    public struct LevelLayer
    {
        public AsyncOperation loadOperation;
    }

    public class Level
    {
        public LevelState state;
        public string name;
        public List<LevelLayer> layers = new List<LevelLayer>(10);
    }

    public class LevelManager
    {
        public static readonly string[] layerNames = new string[]
        {
        "environment",
        "gamePlay",
        };

        public Level currentLevel { get; private set; }

        public void Init()
        {
        }

        public bool IsCurrentLevelLoaded()
        {
            return currentLevel != null && currentLevel.state == LevelState.Loaded;
        }

        public bool IsLoadingLevel()
        {
            return currentLevel != null && currentLevel.state == LevelState.Loading;
        }

        public bool CanLoadLevel(string name)
        {
            AssetBundle bundle = null; // SimpleBundleManager.LoadLevelAssetBundle(name);
            return bundle != null;
        }

        public bool LoadLevel(string name)
        {
            if (currentLevel != null)
                UnloadLevel();

            var newLevel = new Level
            {
                name = name
            };

            AssetBundle bundle = null; //SimpleBundleManager.RequestLoadIgloo(name);
            if (bundle == null)
            {
                Debug.Log("Could not load asset bundle for scene " + name);
                return false;
            }

            // Load using the name found in GetAllScenePaths because SceneManager.LoadSceneAsync is case sensitive
            // yet name may not have correct casing as file system may be case insensitive 

            var scenePaths = new List<string>(bundle.GetAllScenePaths());
            if (scenePaths.Count < 1)
            {
                Debug.Log("No scenes in asset bundle " + name);
                return false;
            }

            // If there is a main scene, load that first
            var mainScenePath = scenePaths.Find(x => x.ToLower().EndsWith("_main.unity"));
            var useLayers = true;
            if (mainScenePath == null)
            {
                useLayers = false;
                mainScenePath = scenePaths[0];
            }

            Debug.Log("Loading " + mainScenePath);
            var mainLoadOperation = SceneManager.LoadSceneAsync(mainScenePath, LoadSceneMode.Single);
            if (mainLoadOperation == null)
            {
                Debug.Log("Failed to load level : " + name);
                return false;
            }

            currentLevel = newLevel;
            currentLevel.layers.Add(new LevelLayer { loadOperation = mainLoadOperation });

            if (!useLayers)
                return true;

            // Now load all additional layers that may be here
            foreach (var l in layerNames)
            {
                var layerScenePath = scenePaths.Find(x => x.ToLower().EndsWith(l + ".unity"));
                if (layerScenePath == null)
                    continue;

                // TODO : Are we guaranteed that the scenes are initialized in order without setting allowactivation = false?
                Debug.Log("+Loading " + layerScenePath);
                var layerLoadOperation = SceneManager.LoadSceneAsync(layerScenePath, LoadSceneMode.Additive);
                if (layerLoadOperation != null)
                {
                    currentLevel.layers.Add(new LevelLayer { loadOperation = layerLoadOperation });
                }
                else
                {
                    Debug.Log("Warning : Unable to load level layer : " + layerScenePath);
                }
            }

            return true;
        }

        public void UnloadLevel()
        {
            if (currentLevel == null)
                return;

            if (currentLevel.state == LevelState.Loading)
                throw new NotImplementedException("TODO : Implement unload during load");

            // TODO : Load empty scene for now
            SceneManager.LoadScene(1);

            //SimpleBundleManager.ReleaseLevelAssetBundle(currentLevel.name);
            currentLevel = null;
        }

        public void Update()
        {
            if (currentLevel != null && currentLevel.state == LevelState.Loading)
            {
                var done = currentLevel.layers.All(l => l.loadOperation.isDone);
                if (done)
                {
                    currentLevel.state = LevelState.Loaded;
                    Debug.Log("Scene " + currentLevel.name + " loaded");
                }
            }
        }


        public enum BuildType
        {
            Default,
            Client,
            Server,
        }

        public static void StripCode(BuildType buildType, bool isDevelopmentBuild)
        {
            Debug.Log("Stripping code for " + buildType.ToString() + " (" + (isDevelopmentBuild ? "DevBuild" : "NonDevBuild") + ")");
            var deleteBehaviors = new List<MonoBehaviour>();
            var deleteGameObjects = new List<GameObject>();

            Debug.Log(string.Format("Stripping {0} game object(s) and {1} behavior(s)", deleteGameObjects.Count, deleteBehaviors.Count));

            foreach (var gameObject in deleteGameObjects)
                UnityEngine.Object.DestroyImmediate(gameObject);

            foreach (var behavior in deleteBehaviors)
                UnityEngine.Object.DestroyImmediate(behavior);
        }
    }
}
