﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Com.IglooVision.Showroom
{
    public class SimpleBundleManager : MonoBehaviour
    {
        public static SimpleBundleManager instance;
        [SerializeField] List<AssetReference> _iglooAssetReferences = new List<AssetReference>();
        [SerializeField] Vector3 _iglooSpawnPosition = Vector3.zero;

        private static readonly Dictionary<AssetReference, AsyncOperationHandle<GameObject>> _asyncOperationHandles =
            new Dictionary<AssetReference, AsyncOperationHandle<GameObject>>();

        private static readonly Dictionary<AssetReference,GameObject> _spawnedIgloos =
            new Dictionary<AssetReference, GameObject>();

        private static GameObject currentLoadedIgloo = null;

        public static float currentDownloadProgress = 0;

        public static AsyncOperationHandle<GameObject> currentOpHandle { get; private set; }

        #region Monobehaviour callbacks

        public void Start()
        {
            instance = this;
        }

        #endregion

        public static void RequestLoadIgloo(int index)
        {
            index = index - 1; // Quick fix, as list starts at 0, not 1.
            if (index < 0 || index >= instance._iglooAssetReferences.Count) { Debug.LogError("SBM::RequestLoadIgloo - No Igloo Index supplied"); return; }
            AssetReference assetReference = instance._iglooAssetReferences[index];
            Debug.Log($"SBM::RequestLoadIgloo - Downloading Igloo {assetReference.SubObjectName}");

            if (assetReference.RuntimeKeyIsValid() == false)
            {
                Debug.LogError($"SBM::RequestLoadIgloo - Invalid Key {assetReference.SubObjectName}");
                return;
            }

            LoadAndSpawnIgloo(assetReference);
        }

        private static void LoadAndSpawnIgloo(AssetReference assetReference)
        {
            Debug.Log($"SBM::LoadAndSpawnIgloo - Loading new Igloo for asset reference: {assetReference.SubObjectName}");
            currentOpHandle = Addressables.LoadAssetAsync<GameObject>(assetReference);
            _asyncOperationHandles[assetReference] = currentOpHandle;
            currentOpHandle.Completed += (operation) =>
            {
                SpawnIglooFromLoadedReference(assetReference);
            };
        }

        private void Update()
        {
            if(currentOpHandle.IsValid() && !currentOpHandle.IsDone)
            {
                currentDownloadProgress = currentOpHandle.PercentComplete;
            }
        }

        private static void SpawnIglooFromLoadedReference(AssetReference assetReference)
        {
            Debug.Log("SBM::SpawnIglooFromLoadedReference - Spawning new Igloo");
            // As we are about to load a new igloo, destroy the previous one. It will automatically kill it's assetReferences
            if(currentLoadedIgloo != null) Destroy(currentLoadedIgloo);
            // Spawn a new Igloo
            assetReference.InstantiateAsync(instance._iglooSpawnPosition, Quaternion.identity).Completed += (asyncOperationHandle) =>
            {
                if (_spawnedIgloos.ContainsKey(assetReference) == false)
                {
                    _spawnedIgloos[assetReference] = asyncOperationHandle.Result;
                    currentLoadedIgloo = asyncOperationHandle.Result;
                    var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
                    notify.Destroyed += Remove;
                    notify.AssetReference = assetReference;
                    Debug.Log($"SBM::SpawnIglooFromLoadedReference - {currentLoadedIgloo} Loaded");
                    //onLoadedIglooComplete(asyncOperationHandle.Result);
                }
                else
                {
                    Debug.Log("SBM::SpawnIglooFromLoadedReference - Igloo is already loaded, doing nothing");
                }
            };
        }

        private static void Remove(AssetReference assetReference, NotifyOnDestroy obj)
        {
            Addressables.ReleaseInstance(obj.gameObject);

            if (_asyncOperationHandles[assetReference].IsValid())
            {
                Addressables.Release(_asyncOperationHandles[assetReference]);
            }
            _asyncOperationHandles.Remove(assetReference);
        }
    }
}
