﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

using Photon.Pun.Demo.Shared;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Pun;

namespace Com.IglooVision.Showroom
{
    public class IglooShowroomFrontend : MonoBehaviourPunCallbacks
    {
        #region Static Variables
        public static IglooShowroomFrontend Instance;
        public static bool Embedded;
        public static string EmbeddedGameTitle = "";
        #endregion

        #region Public Variables
        public bool debug = false;
        public string UserId { get; set; }


        public Text Title;
        public Text StateText; // set in inspector
        public Text UserIdText; // set in inspector
        #endregion

        #region Private Variables
        [SerializeField, Header("Showroom Integration")]
        private CanvasGroup MinimalCanvasGroup;
        [SerializeField] private CanvasGroup MaximalCanvasGroup;
        [SerializeField] private GameObject MinimizeButton;
        [SerializeField] private GameObject MinimalUIEmbeddHelp;

        [SerializeField, Header("Connection UI")]
        private GameObject ConnectingLabel;
        [SerializeField] private GameObject ConnectionPanel;
        [SerializeField] private GameObject AdvancedConnectionPanel;
        [SerializeField] private Dropdown ConnectAsDropDown;

        [SerializeField, Header("Common UI")]
        private GameObject InfosPanel;
        [SerializeField] private GameObject MinimalUiInfosPanel;

        [SerializeField, Header("Lobby UI")]
        private GameObject LobbyPanel;
        [SerializeField] private Selectable JoinLobbyButton;
        [SerializeField] private FriendListView FriendListManager;
        [SerializeField] private RoomListView RoomListManager;
        [SerializeField] private GameObject RoomListMatchMakingForm;

        [SerializeField, Header("Game UI")]
        private GameObject GamePanel;
        [SerializeField] private PlayerListView PlayerListManager;
        [SerializeField] private PlayerDetailsController PlayerDetailsManager;

        [SerializeField] private InputField RoomCustomPropertyInputfield;

        [SerializeField, Header("Photon Settings")]
        /// <summary>
        /// The game version override. This is one way to let the user define the gameversion, and set it properly right after we call connect to override the server settings
        /// Check ConnectAndJoinRandom.cs for another example of gameversion overriding
        /// </summary>
		private string GameVersionOverride = String.Empty;

        /// <summary>
        /// The reset flag for best cloud ServerSettings.
        /// This is one way to let the user define if bestcloud cache should be reseted when connecting.
        /// </summary>
		[SerializeField] private bool ResetBestRegionCodeInPreferences = false;

        [SerializeField, Header("Room Options")]
        private int MaxPlayers = 4;
        [SerializeField] private int PlayerTtl = 0;
        [SerializeField] private int EmptyRoomTtl = 0;
        [SerializeField] private string Plugins = "";
        [SerializeField] private bool PublishUserId = true;
        [SerializeField] private bool IsVisible = true;
        [SerializeField] private bool IsOpen = true;
        [SerializeField] private bool CleanupCacheOnLeave = true;
        [SerializeField] private bool DeleteNullProperties = false;

        [SerializeField, Header("Room Options UI")]
        private IntInputField PlayerTtlField;
        [SerializeField] private IntInputField EmptyRoomTtlField;
        [SerializeField] private IntInputField MaxPlayersField;
        [SerializeField] private StringInputField PluginsField;
        [SerializeField] private BoolInputField PublishUserIdField;
        [SerializeField] private BoolInputField IsVisibleField;
        [SerializeField] private BoolInputField IsOpenField;
        [SerializeField] private BoolInputField CleanupCacheOnLeaveField;
        [SerializeField] private BoolInputField DeleteNullPropertiesField;

        [SerializeField, Header("Friends Options")]
        private FriendListView.FriendDetail[] FriendsList =
            new FriendListView.FriendDetail[]{};


        [SerializeField, Header("Modal window")]
        private CanvasGroup ModalWindow;
        [SerializeField] private RegionListView RegionListView;
        [SerializeField] private Text RegionListLoadingFeedback;

        [SerializeField] private LoadBalancingClient _lbc;
        [SerializeField] private bool _regionPingProcessActive;
        [SerializeField] private List<Region> RegionsList;
        #endregion

        #region Mono Callbacks

        public void Start()
        {

            Instance = this;

            // doc setup

            DocLinks.Language = DocLinks.Languages.English;
            DocLinks.Product = DocLinks.Products.Pun;
            DocLinks.Version = DocLinks.Versions.V2;

            //

            ModalWindow.gameObject.SetActive(false);

            MaximalCanvasGroup.gameObject.SetActive(true);

            this.UserIdText.text = "";
            this.StateText.text = "";
            this.StateText.gameObject.SetActive(true);
            this.UserIdText.gameObject.SetActive(true);
            this.Title.gameObject.SetActive(true);

            this.ConnectingLabel.SetActive(false);
            this.LobbyPanel.SetActive(false);
            this.GamePanel.SetActive(false);

            if (string.IsNullOrEmpty(UserId))
            {
                UserId = "user" + Environment.TickCount % 99; //made-up username
            }

            PlayerTtlField.SetValue(this.PlayerTtl);
            EmptyRoomTtlField.SetValue(this.EmptyRoomTtl);
            MaxPlayersField.SetValue(this.MaxPlayers);
            PluginsField.SetValue(this.Plugins);
            PublishUserIdField.SetValue(this.PublishUserId);
            IsVisibleField.SetValue(this.IsVisible);
            IsOpenField.SetValue(this.IsOpen);
            CleanupCacheOnLeaveField.SetValue(this.CleanupCacheOnLeave);
            //CheckUserOnJoinField.SetValue (this.CheckUserOnJoin);
            DeleteNullPropertiesField.SetValue(this.DeleteNullProperties);



            // prefill dropdown selection of users
            ConnectAsDropDown.ClearOptions();
            ConnectAsDropDown.AddOptions(FriendsList.Select(x => x.NickName).ToList());


            // check the current network status

            if (PhotonNetwork.IsConnected)
            {
                if (PhotonNetwork.Server == ServerConnection.GameServer)
                {
                    this.OnJoinedRoom();

                }
                else if (PhotonNetwork.Server == ServerConnection.MasterServer || PhotonNetwork.Server == ServerConnection.NameServer)
                {

                    if (PhotonNetwork.InLobby)
                    {
                        this.OnJoinedLobby();
                    }
                    else
                    {
                        this.OnConnectedToMaster();
                    }

                }
            }
            else
            {
                this.SwitchToSimpleConnection();

                if (!Embedded)
                {
                    MinimizeButton.SetActive(false);
                    SwitchtoMaximalPanel();
                }
                else
                {
                    this.Title.text = EmbeddedGameTitle;
                    SwitchtoMinimalPanel();
                }
            }
        }

        void Update()
        {
            if (_lbc != null) _lbc.Service();

            if (RegionsList != null)
            {
                if (this.ModalWindow.gameObject.activeInHierarchy)
                {

                    if (IglooShowroomFrontend.Instance.debug) Debug.Log("PunCockpit:OnRegionsPinged");

                    this.RegionListView.OnRegionListUpdate(RegionsList);
                }

                _lbc = null;

                RegionListLoadingFeedback.text = string.Empty;

                RegionsList = null;
            }
        }

        #endregion

        #region PUN CallBacks

        public override void OnRegionListReceived(RegionHandler regionHandler)
        {
            if (IglooShowroomFrontend.Instance.debug)
                Debug.Log("PunCockpit:OnRegionListReceived: " + regionHandler);

            if (_regionPingProcessActive)
            {
                RegionListLoadingFeedback.text = "Pinging Regions...";
                _regionPingProcessActive = false;
                regionHandler.PingMinimumOfRegions(OnRegionsPinged, null);
            }
        }

        public override void OnConnected()
        {
            if (debug) Debug.Log("PunCockpit:OnConnected()");

            this.ConnectingLabel.SetActive(false);

            this.UserIdText.text = "UserId:" + this.UserId + " Nickname:" + PhotonNetwork.NickName;
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            if (debug) Debug.Log("PunCockpit:OnDisconnected(" + cause + ")");

            this.ConnectingLabel.SetActive(false);
            this.UserIdText.text = string.Empty;
            this.StateText.text = string.Empty;

            this.GamePanel.gameObject.SetActive(false);
            this.LobbyPanel.gameObject.SetActive(false);
            this.ConnectionPanel.gameObject.SetActive(true);

        }

        public override void OnConnectedToMaster()
        {
            if (debug) Debug.Log("PunCockpit:OnConnectedToMaster()");


            this.StateText.text = "Connected to Master" + (PhotonNetwork.OfflineMode ? " <Color=Red><b>Offline</b></color>" : "");

            this.SetUpLobbyGenericUI();
        }

        public override void OnJoinedLobby()
        {
            this.StateText.text = "Connected to Lobby";

            if (debug) Debug.Log("PunCockpit:OnJoinedLobby()");
            this.SetUpLobbyGenericUI();
        }

        void SetUpLobbyGenericUI()
        {
            this.ConnectingLabel.gameObject.SetActive(false);
            this.AdvancedConnectionPanel.gameObject.SetActive(false);
            this.LobbyPanel.gameObject.SetActive(true);
            this.RoomListManager.OnJoinedLobbyCallBack();
            this.FriendListManager.SetFriendDetails(this.FriendsList);

            JoinLobbyButton.interactable = !PhotonNetwork.InLobby && !PhotonNetwork.OfflineMode;


            RoomListManager.gameObject.SetActive(!PhotonNetwork.OfflineMode);
            FriendListManager.gameObject.SetActive(!PhotonNetwork.OfflineMode);

            RoomListMatchMakingForm.SetActive(!PhotonNetwork.InLobby);
        }

        public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
        {
            if (debug) Debug.Log("PunCockpit:OnRoomPropertiesUpdate() " + propertiesThatChanged.ToStringFull());

            if (propertiesThatChanged.ContainsKey("C0"))
            {
                RoomCustomPropertyInputfield.text = propertiesThatChanged["C0"].ToString();
            }
        }

        public override void OnLeftLobby()
        {
            if (debug) Debug.Log("PunCockpit:OnLeftLobby()");

            this.RoomListManager.ResetList();
            this.LobbyPanel.gameObject.SetActive(false);
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            if (debug) Debug.Log("PunCockpit:OnCreateRoomFailed(" + returnCode + "," + message + ")");
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            if (debug) Debug.Log("PunCockpit:OnJoinRandomFailed(" + returnCode + "," + message + ")");
        }

        public override void OnJoinedRoom()
        {

            this.StateText.text = "Connected to GameServer " + (PhotonNetwork.OfflineMode ? " <Color=Red><b>Offline</b></color>" : "");


            if (debug) Debug.Log("PunCockpit:OnJoinedRoom()");

            this.ConnectingLabel.gameObject.SetActive(false);

            this.PlayerListManager.ResetList();

            this.GamePanel.gameObject.SetActive(true);

            this.PlayerDetailsManager.SetPlayerTarget(PhotonNetwork.LocalPlayer);

        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            switch (returnCode)
            {
                case ErrorCode.JoinFailedFoundInactiveJoiner:
                    if (!string.IsNullOrEmpty(this.roomNameToEnter))
                    {
                        PhotonNetwork.RejoinRoom(this.roomNameToEnter);
                        this.roomNameToEnter = null;
                    }
                    break;
            }
        }

        public override void OnLeftRoom()
        {
            if (debug) Debug.Log("PunCockpit:OnLeftRoom()");
            this.GamePanel.gameObject.SetActive(false);

            if (PhotonNetwork.OfflineMode)
            {
                this.ConnectingLabel.gameObject.SetActive(false);
                this.ConnectionPanel.gameObject.SetActive(true);
            }
        }

        #endregion

        #region Custom Callbacks

        private void OnRegionsPinged(RegionHandler regionHandler)
        {
            RegionsList = regionHandler.EnabledRegions.OrderBy(x => x.Ping).ToList();
            // will check this on Update() to get back to the main thread.

        }

        public void CloseRegionListView()
        {

            RegionsList = null;

            if (_lbc != null)
            {
                _lbc.Disconnect();
                _lbc = null;
            }

            _regionPingProcessActive = false;

            this.RegionListView.ResetList();
            this.ModalWindow.gameObject.SetActive(false);
        }

        public void LoadLevel(string level)
        {
            if (debug) Debug.Log("PunCockpit:LoadLevel(" + level + ")");
            PhotonNetwork.LoadLevel(level);
        }

        public void SetRoomCustomProperty(string value)
        {
            if (debug) Debug.Log("PunCockpit:SetRoomCustomProperty() c0 = " + value);
            PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() { { "C0", value } });
        }

        private string roomNameToEnter;

        public void JoinRoom(string roomName)
        {
            this.RoomListManager.ResetList();
            this.LobbyPanel.gameObject.SetActive(false);
            this.ConnectingLabel.SetActive(true);
            this.roomNameToEnter = roomName;
            PhotonNetwork.JoinRoom(roomName);
        }

        public void CreateRoom()
        {
            this.CreateRoom(null, null, LobbyType.Default);
        }

        public void CreateRoom(string roomName, string lobbyName = "MyLobby", LobbyType lobbyType = LobbyType.SqlLobby, string[] expectedUsers = null)
        {
            if (debug) Debug.Log("PunCockpit:CreateRoom roomName:" + roomName + " lobbyName:" + lobbyName + " lobbyType:" + lobbyType + " expectedUsers:" + (expectedUsers == null ? "null" : expectedUsers.ToStringFull()));

            this.RoomListManager.ResetList();
            this.LobbyPanel.gameObject.SetActive(false);
            this.ConnectingLabel.SetActive(true);

            RoomOptions _roomOptions = this.GetRoomOptions();
            if (debug) Debug.Log("PunCockpit:Room options  <" + _roomOptions + ">");

            TypedLobby sqlLobby = new TypedLobby(lobbyName, lobbyType);
            bool _result = PhotonNetwork.CreateRoom(roomName, _roomOptions, sqlLobby, expectedUsers);

            if (debug) Debug.Log("PunCockpit:CreateRoom() -> " + _result);

        }

        public void JoinRandomRoom()
        {
            PhotonNetwork.JoinRandomRoom();
        }

        public void LeaveRoom()
        {
            PlayerListManager.ResetList();
            this.GamePanel.gameObject.SetActive(false);
            this.ConnectingLabel.SetActive(true);

            PhotonNetwork.LeaveRoom();

        }

        public void Connect()
        {
            this.ConnectionPanel.gameObject.SetActive(false);
            this.AdvancedConnectionPanel.gameObject.SetActive(false);

            PhotonNetwork.AuthValues = new AuthenticationValues();
            PhotonNetwork.AuthValues.UserId = this.UserId;

            this.ConnectingLabel.SetActive(true);

            PhotonNetwork.ConnectUsingSettings();
            //if (GameVersionOverride != string.Empty) {
            //		PhotonNetwork.GameVersion = "28"; // GameVersionOverride;
            //	}
        }

        public void ReConnect()
        {
            this.ConnectionPanel.gameObject.SetActive(false);
            this.AdvancedConnectionPanel.gameObject.SetActive(false);

            PhotonNetwork.AuthValues = new AuthenticationValues();
            PhotonNetwork.AuthValues.UserId = this.UserId;

            this.ConnectingLabel.SetActive(true);

            PhotonNetwork.Reconnect();
        }

        public void ReconnectAndRejoin()
        {
            this.ConnectionPanel.gameObject.SetActive(false);
            this.AdvancedConnectionPanel.gameObject.SetActive(false);

            PhotonNetwork.AuthValues = new AuthenticationValues();
            PhotonNetwork.AuthValues.UserId = this.UserId;

            this.ConnectingLabel.SetActive(true);

            PhotonNetwork.ReconnectAndRejoin();
        }


        public void ConnectToBestCloudServer()
        {

            PhotonNetwork.NetworkingClient.AppId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime;

            this.ConnectionPanel.gameObject.SetActive(false);
            this.AdvancedConnectionPanel.gameObject.SetActive(false);

            PhotonNetwork.AuthValues = new AuthenticationValues();
            PhotonNetwork.AuthValues.UserId = this.UserId;

            this.ConnectingLabel.SetActive(true);

            if (this.ResetBestRegionCodeInPreferences)
            {
                ServerSettings.ResetBestRegionCodeInPreferences();
            }

            PhotonNetwork.ConnectToBestCloudServer();
            if (GameVersionOverride != string.Empty)
            {
                PhotonNetwork.GameVersion = GameVersionOverride;
            }
        }

        public void ConnectToRegion(string region)
        {

            if (debug) Debug.Log("PunCockpit:ConnectToRegion(" + region + ")");

            PhotonNetwork.NetworkingClient.AppId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime;

            this.ConnectionPanel.gameObject.SetActive(false);
            this.AdvancedConnectionPanel.gameObject.SetActive(false);

            PhotonNetwork.AuthValues = new AuthenticationValues();
            PhotonNetwork.AuthValues.UserId = this.UserId;

            this.ConnectingLabel.SetActive(true);

            bool _result = PhotonNetwork.ConnectToRegion(region);

            if (GameVersionOverride != string.Empty)
            {
                PhotonNetwork.GameVersion = GameVersionOverride;
            }

            if (debug) Debug.Log("PunCockpit:ConnectToRegion(" + region + ") ->" + _result);
        }



        public void ConnectOffline()
        {
            if (debug) Debug.Log("PunCockpit:ConnectOffline()");
            PhotonNetwork.OfflineMode = true;
        }

        public void JoinLobby()
        {
            if (debug) Debug.Log("PunCockpit:JoinLobby()");
            bool _result = PhotonNetwork.JoinLobby();

            if (!_result)
            {
                Debug.LogError("PunCockpit: Could not joinLobby");
            }

        }

        public void Disconnect()
        {
            if (debug) Debug.Log("PunCockpit:Disconnect()");
            PhotonNetwork.Disconnect();
        }


        public void OpenDashboard()
        {
            Application.OpenURL("https://dashboard.photonengine.com");
        }


        #region CONNECT UI
        public void OnDropdownConnectAs(int dropdownIndex)
        {
            if (debug) Debug.Log("PunCockpit:OnDropdownConnectAs(" + dropdownIndex + ")");

            this.UserId = this.FriendsList[dropdownIndex].UserId;
            PlayerPrefs.SetString(UserIdUiForm.UserIdPlayerPref, this.UserId);

            StartCoroutine(OnDropdownConnectAs_CB());
        }

        IEnumerator OnDropdownConnectAs_CB()
        {
            // wait for the dropdown to animate.
            yield return new WaitForSeconds(0.2f);

            this.Connect();
        }

        #endregion
        #region IN LOBBY UI

        public void OnLobbyToolsViewTabChanged(string tabId)
        {
            //	Debug.Log("PunCockpit:OnLobbyToolsViewTabChanged("+tabId+")");
        }


        #endregion

        #region IN ROOM UI 

        public void OnSelectPlayer()
        {

        }

        #endregion

        void OnStateChanged(ClientState previousState, ClientState state)
        {
            if (state == ClientState.ConnectedToNameServer)
            {
                _lbc.StateChanged -= this.OnStateChanged;

                if (debug) Debug.Log("PunCockpit:OnStateChanged: ClientState.ConnectedToNameServer. Waiting for OnRegionListReceived callback.");

                RegionListLoadingFeedback.text = "Waiting for application Region List...";
            }
        }

        public void SwitchtoMinimalPanel()
        {
            MinimalCanvasGroup.gameObject.SetActive(true);
            MaximalCanvasGroup.alpha = 0f;
            MaximalCanvasGroup.blocksRaycasts = false;
            MaximalCanvasGroup.interactable = false;
        }

        public void SwitchtoMaximalPanel()
        {
            MinimalUIEmbeddHelp.SetActive(false);
            MinimalCanvasGroup.gameObject.SetActive(false);

            MaximalCanvasGroup.alpha = 1f;
            MaximalCanvasGroup.blocksRaycasts = true;
            MaximalCanvasGroup.interactable = true;
        }

        public void SwitchToAdvancedConnection()
        {
            this.ConnectionPanel.gameObject.SetActive(false);
            this.AdvancedConnectionPanel.gameObject.SetActive(true);
        }

        public void SwitchToSimpleConnection()
        {
            this.ConnectionPanel.gameObject.SetActive(true);
            this.AdvancedConnectionPanel.gameObject.SetActive(false);
        }

        public void ToggleInfosInMinimalPanel()
        {
            MinimalUiInfosPanel.SetActive(!MinimalUiInfosPanel.activeSelf);
        }

        public void RequestInfosPanel(GameObject Parent)
        {
            if (Parent != null)
            {
                InfosPanel.transform.SetParent(Parent.transform, false);
            }
        }

        public void OnUserIdSubmited(string userId)
        {
            this.UserId = userId;
            this.Connect();
        }

        public void SetPlayerTtlRoomOption(int value)
        {
            this.PlayerTtl = value;
            if (debug) Debug.Log("PunCockpit:PlayerTtl = " + this.PlayerTtl);
        }

        public void SetEmptyRoomTtlRoomOption(int value)
        {
            this.EmptyRoomTtl = value;
            if (debug) Debug.Log("PunCockpit:EmptyRoomTtl = " + this.EmptyRoomTtl);
        }

        public void SetMaxPlayersRoomOption(int value)
        {
            this.MaxPlayers = value;
            if (debug) Debug.Log("PunCockpit:MaxPlayers = " + this.MaxPlayers);
        }

        public void SetPluginsRoomOption(string value)
        {
            this.Plugins = value;
            if (debug) Debug.Log("PunCockpit:Plugins = " + this.Plugins);
        }

        public void SetPublishUserId(bool value)
        {
            this.PublishUserId = value;
            if (debug) Debug.Log("PunCockpit:PublishUserId = " + this.PublishUserId);
        }

        public void SetIsVisible(bool value)
        {
            this.IsVisible = value;
            if (debug) Debug.Log("PunCockpit:IsVisible = " + this.IsVisible);
        }

        public void SetIsOpen(bool value)
        {
            this.IsOpen = value;
            if (debug) Debug.Log("PunCockpit:IsOpen = " + this.IsOpen);
        }

        //	public void SetCheckUserOnJoin(bool value)
        //	{
        //		this.CheckUserOnJoin = value;
        //		Debug.Log ("CheckUserOnJoin = " + this.CheckUserOnJoin);
        //	}

        public void SetResetBestRegionCodeInPreferences(bool value)
        {
            this.ResetBestRegionCodeInPreferences = value;
            if (debug) Debug.Log("PunCockpit:ResetBestRegionCodeInPreferences = " + this.ResetBestRegionCodeInPreferences);
        }

        public void SetCleanupCacheOnLeave(bool value)
        {
            this.CleanupCacheOnLeave = value;
            if (debug) Debug.Log("PunCockpit:CleanupCacheOnLeave = " + this.CleanupCacheOnLeave);
        }

        public void SetDeleteNullProperties(bool value)
        {
            this.DeleteNullProperties = value;
            if (debug) Debug.Log("PunCockpit:DeleteNullProperties = " + this.DeleteNullProperties);
        }

        /// <summary>
        /// in progress, not fully working
        /// </summary>
        public void PingRegions()
        {
            ModalWindow.gameObject.SetActive(true);

            RegionListLoadingFeedback.text = "Connecting to NameServer...";
            _regionPingProcessActive = true;
            if (debug) Debug.Log("PunCockpit:PingRegions:ConnectToNameServer");


            _lbc = new LoadBalancingClient();

            _lbc.AddCallbackTarget(this);


            _lbc.StateChanged += OnStateChanged;

            _lbc.AppId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime;
            _lbc.ConnectToNameServer();

        }

        RoomOptions GetRoomOptions()
        {
            RoomOptions _roomOptions = new RoomOptions();

            _roomOptions.MaxPlayers = (byte)this.MaxPlayers;

            _roomOptions.IsOpen = this.IsOpen;

            _roomOptions.IsVisible = this.IsVisible;

            _roomOptions.EmptyRoomTtl = this.EmptyRoomTtl;

            _roomOptions.PlayerTtl = this.PlayerTtl;

            _roomOptions.PublishUserId = this.PublishUserId;

            _roomOptions.CleanupCacheOnLeave = this.CleanupCacheOnLeave;
            _roomOptions.DeleteNullProperties = this.DeleteNullProperties;

            _roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() { { "C0", "Hello" } };
            _roomOptions.CustomRoomPropertiesForLobby = new string[] { "C0" };


            return _roomOptions;
        }

        #endregion
    }
}
