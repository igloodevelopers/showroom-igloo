﻿using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.Demo.Shared;
using ExitGames.Client.Photon;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;

namespace Com.IglooVision.Showroom
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
        public static Launcher Instance;
        #region Private fields
        /// <summary>
        /// This client's version number. Users are seperated between game versions.
        /// </summary>
        string gameVersion = "1";

        /// <summary>
        /// keeps track of current progress, since the connection is Async and based on internet connection, pc speed, and other factors.
        /// Typically this is used for OnConnectedToMaster() callback.
        /// </summary>
        bool isConnecting;

        private enum CurrentScreen { Login, Lobby, CreateRoom}
        private CurrentScreen currentScreen = CurrentScreen.Lobby;
        #endregion

        #region Public fields
        /// <summary>
        /// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
        /// </summary>
        [Header("UI Options"), SerializeField, Tooltip("The UI Panel to let the user enter their name, and connect")]
        private GameObject loginControlPanel;

        [SerializeField, Tooltip("The UI Label to inform the user that the connection is in progress")]
        private Text connectingLabel;

        [Tooltip("The UI Label that explains the current situation")]
        public Text StateText; // set in inspector

        [SerializeField, Tooltip("The root object of the Lobby system")]
        private GameObject LobbyPanel;

        [SerializeField, Tooltip("The root object of the entire UI")]
        private GameObject ConnectionPanel;

        [Header("User ID system & login")]
        public Text UserIdText; // set in inspector

        public string UserId { get; set; }

        public const string UserIdPlayerPref = "PunUserId";

        public const string ELO_PROP_KEY = "C0";
        public const string MAP_PROP_KEY = "C1";

        public InputField idInput;

        [System.Serializable]
        public class OnSubmitEvent : UnityEvent<string> { }
        public OnSubmitEvent OnSubmit;

        [Header("Room Options UI")]
        public RoomListView RoomListManager;
        public GameObject CreateRoomOptionsObject;
        public StringInputField EmptyRoomTtlField;
        private string RoomName = "New Room";
        public IntInputField MaxPlayersField;
        public InputField streamURLInputField;
        private string streamURL = "";
        private int MaxPlayers = 4;
        #endregion

        #region Mono CallBacks

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity once the scene is loaded. 
        /// </summary>
        private void Start()
        {
            Instance = this;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            string prefsName = PlayerPrefs.GetString(UserIdUiForm.UserIdPlayerPref);
            if (!string.IsNullOrEmpty(prefsName))
            {
                this.idInput.text = prefsName;
                UserIdText.text = prefsName;
            }
            currentScreen = CurrentScreen.Login;

            string prefsURL = PlayerPrefs.GetString(UserIdUiForm.UserURLPlayerPref);
            if (!string.IsNullOrEmpty(prefsURL))
            {
                this.streamURLInputField.text = prefsURL;
                streamURL = prefsURL;
            }
            // doc setup
            DocLinks.Language = DocLinks.Languages.English;
            DocLinks.Product = DocLinks.Products.Pun;
            DocLinks.Version = DocLinks.Versions.V2;

            // Make sure only the login panel is active on Start.
            this.connectingLabel.gameObject.SetActive(false);
            this.LobbyPanel.SetActive(false);
            this.CreateRoomOptionsObject.SetActive(false);

            PhotonNetwork.AutomaticallySyncScene = true;

            this.loginControlPanel.SetActive(true);
        }
        #endregion

        #region MonoBehaviourPunCallBacks Callbacks

        public override void OnDisconnected(DisconnectCause cause)
        {
            connectingLabel.gameObject.SetActive(false);
            StateText.text = "";
            isConnecting = false;
            loginControlPanel.SetActive(true);
            Debug.LogWarningFormat("IglooShowroom/Launcher: OnDisconnected() with reason {0}", cause);
        }

        public override void OnJoinedRoom()
        {
            Debug.Log($"IglooShowroom:OnJoinedRoom: Current player count: {PhotonNetwork.CurrentRoom.PlayerCount}");
            this.connectingLabel.gameObject.SetActive(false);
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                // UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(1);
                Addressables.LoadSceneAsync("Assets/Level_01_Main");
                Debug.Log($"IglooShowroom:OnJoinedRoom: Loading scene Level_01_Main from addressables.");
            }
        }

        #endregion

        #region Public Methods

        private string roomNameToEnter;

        private void Update()
        {
            if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
            {
                switch (currentScreen)
                {
                    case CurrentScreen.Login:
                        if(UserId != "")
                            this.SubmitForm();
                        break;
                    case CurrentScreen.Lobby:
                        // Nowt to do here.
                        break;
                    case CurrentScreen.CreateRoom:
                        this.CreateNewRoom();
                        break;
                }
            }
        }

        public void SubmitForm()
        {
            PlayerPrefs.SetString(UserIdUiForm.UserIdPlayerPref, idInput.text);
            OnSubmit.Invoke(idInput.text);
            currentScreen = CurrentScreen.Lobby;
        }

        public void OnSubmitNewURL(string newURL)
        {
            PlayerPrefs.SetString(UserIdUiForm.UserURLPlayerPref, newURL);
            streamURL = newURL;
            Debug.Log($"NewURL::{newURL}");
        }

        public void JoinRoom(string roomName)
        {
            this.RoomListManager.ResetList();
            this.LobbyPanel.gameObject.SetActive(false);
            this.connectingLabel.gameObject.SetActive(true);
            this.roomNameToEnter = roomName;
            PhotonNetwork.JoinRoom(roomName);
        }

        public void SetMaxPlayersRoomOption(int value)
        {
            this.MaxPlayers = value;
            Debug.Log("IglooShowroom:MaxPlayers = " + this.MaxPlayers);
        }

        public void OnEmptyRoomTtlFieldSubmitted(string newRoomID)
        {
            this.RoomName = newRoomID;
            Debug.Log("IglooShowroom:NewRoomID = " + this.RoomName);
        }

        public void OnUserIdSubmited(string userId)
        {
            this.UserId = userId;
            this.Connect();
        }

        /// <summary>
        /// When 'true' will turn on the create room form, and turn off the matchmaking form. 
        /// </summary>
        /// <param name="isOn"></param>
        public void ToggleCreateRoomPanel(bool isOn)
        {
            LobbyPanel.SetActive(!isOn);
            currentScreen = isOn ? CurrentScreen.CreateRoom : CurrentScreen.Lobby;
            CreateRoomOptionsObject.SetActive(isOn);
        }

        /// <summary>
        /// Called from UI button, innitiates the create room function, 
        /// using the information entered within the input fields.
        /// </summary>
        public void CreateNewRoom()
        {
            this.CreateRoom(RoomName, "MyLobby", LobbyType.SqlLobby);
        }

        public void CreateRoom(string roomName, string lobbyName = "MyLobby", LobbyType lobbyType = LobbyType.SqlLobby, string[] expectedUsers = null)
        {
            this.RoomListManager.ResetList();
            this.LobbyPanel.gameObject.SetActive(false);
            this.CreateRoomOptionsObject.SetActive(false);
            this.connectingLabel.gameObject.SetActive(true);

            RoomOptions _roomOptions = this.GetRoomOptions();

            TypedLobby sqlLobby = new TypedLobby(lobbyName, lobbyType);
            bool _result = PhotonNetwork.CreateRoom(roomName, _roomOptions, sqlLobby, expectedUsers);
        }

        /// <summary>
        /// Start the connection process.
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
            this.loginControlPanel.gameObject.SetActive(false);
            // this.AdvancedConnectionPanel.gameObject.SetActive(false);

            PhotonNetwork.AuthValues = new AuthenticationValues
            {
                UserId = this.UserId
            };
            UserIdText.text = this.UserId;
            this.connectingLabel.gameObject.SetActive(true);

            PhotonNetwork.ConnectUsingSettings();
        }

        public void Disconnect()
        {
            // Toggle the menu to return back to the start.
            //this.progressLabel.gameObject.SetActive(false);
            this.LobbyPanel.SetActive(false);
            this.CreateRoomOptionsObject.SetActive(false);
            this.loginControlPanel.SetActive(true);
            currentScreen = CurrentScreen.Login;
            isConnecting = false;

            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.Disconnect();
            }
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("IglooShowroom:OnConnectedToMaster()");


            this.StateText.text = "Connected to Master" + (PhotonNetwork.OfflineMode ? " <Color=Red><b>Offline</b></color>" : "");

            this.SetUpLobbyGenericUI();
        }

        public override void OnJoinedLobby()
        {
            this.StateText.text = "Connected to Lobby";

            Debug.Log("IglooShowroom:OnJoinedLobby()");
            this.SetUpLobbyGenericUI();
        }

        void SetUpLobbyGenericUI()
        {
            this.connectingLabel.gameObject.SetActive(false);
            this.loginControlPanel.gameObject.SetActive(false);
            this.LobbyPanel.gameObject.SetActive(true);
            this.RoomListManager.OnJoinedLobbyCallBack();

            RoomListManager.gameObject.SetActive(!PhotonNetwork.OfflineMode);
            LobbyPanel.SetActive(!PhotonNetwork.InLobby);
        }

        public override void OnLeftLobby()
        {
            Debug.Log("IglooShowroom:OnLeftLobby()");

            this.RoomListManager.ResetList();
            this.LobbyPanel.gameObject.SetActive(false);
            this.loginControlPanel.SetActive(true);
            UserIdText.text = this.UserId;

        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("IglooShowroom:OnCreateRoomFailed(" + returnCode + "," + message + ")");
        }

        RoomOptions GetRoomOptions()
        {
            RoomOptions _roomOptions = new RoomOptions
            {
                MaxPlayers = (byte)this.MaxPlayers,
                IsOpen = true,
                IsVisible = true,
                EmptyRoomTtl = 0,
                PlayerTtl = 0,
                PublishUserId = true,
                CleanupCacheOnLeave = true,
                DeleteNullProperties = false,
                CustomRoomProperties = new Hashtable() { { "C0", "Hello" } },
                CustomRoomPropertiesForLobby = new string[] { "C0" }
        };
            return _roomOptions;
        }

        #endregion
    }
}

