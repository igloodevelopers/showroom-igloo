﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;

namespace Com.IglooVision.Showroom
{
    /// <summary>
    /// Room list UI View.
    /// </summary>
    public class RoomListView : MonoBehaviourPunCallbacks
    {
        [System.Serializable]
        public class OnJoinRoomEvent : UnityEvent<string> { }

        public OnJoinRoomEvent OnJoinRoom;

        public RoomListCell CellPrototype;

        public Text UpdateStatusText;

        public Text ContentFeedback;

        bool _firstUpdate = true;

        Dictionary<string, RoomListCell> roomCellList = new Dictionary<string, RoomListCell>();


        public override void OnEnable()
        {
            base.OnEnable();

            ResetList();
            CellPrototype.gameObject.SetActive(false);
        }



        public void OnRoomCellJoinButtonClick(string roomName)
        {
            OnJoinRoom.Invoke(roomName);
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            UpdateStatusText.text = "Updated";

            if (roomList.Count == 0 && !PhotonNetwork.InLobby)
            {
                ContentFeedback.text = "No Rooms found in lobby";
            }

            foreach (RoomInfo entry in roomList)
            {
                if (roomCellList.ContainsKey(entry.Name))
                {
                    if (entry.RemovedFromList)
                    {
                        // we delete the cell
                        roomCellList[entry.Name].RemoveFromList();
                        roomCellList.Remove(entry.Name);
                    }
                    else
                    {
                        // we update the cell
                        roomCellList[entry.Name].RefreshInfo(entry);
                    }

                }
                else
                {
                    if (!entry.RemovedFromList)
                    {
                        // we create the cell
                        roomCellList[entry.Name] = Instantiate(CellPrototype);
                        roomCellList[entry.Name].gameObject.SetActive(true);
                        roomCellList[entry.Name].transform.SetParent(CellPrototype.transform.parent, false);
                        roomCellList[entry.Name].AddToList(entry, !_firstUpdate);
                    }
                }
            }

            StartCoroutine("ClearStatus");

            _firstUpdate = false;
        }

        IEnumerator ClearStatus()
        {
            yield return new WaitForSeconds(1f);

            UpdateStatusText.text = string.Empty;
        }

        IEnumerator GetRoomListDelayed()
        {
            yield return new WaitForSeconds(1f);
            GetRoomList();
        }

        public void OnJoinedLobbyCallBack()
        {
            _firstUpdate = true;
            ContentFeedback.text = string.Empty;
            StartCoroutine(GetRoomListDelayed());
        }

        public void GetRoomList()
        {
            ResetList();

            TypedLobby sqlLobby = new TypedLobby("MyLobby", LobbyType.SqlLobby);

            PhotonNetwork.GetCustomRoomList(sqlLobby, "C0 = 'Hello'");

            ContentFeedback.text = "looking for Rooms in Lobby";
        }


        public void ResetList()
        {
            _firstUpdate = true;

            foreach (KeyValuePair<string, RoomListCell> entry in roomCellList)
            {

                if (entry.Value != null)
                {
                    Destroy(entry.Value.gameObject);
                }

            }
            roomCellList = new Dictionary<string, RoomListCell>();
        }
    }
}
