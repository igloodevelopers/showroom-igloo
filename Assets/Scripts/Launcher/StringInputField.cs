﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Com.IglooVision.Showroom
{
    public class StringInputField : MonoBehaviour
    {
        public InputField PropertyValueInput;

        [System.Serializable]
        public class OnSubmitEvent : UnityEvent<string> { }

        public OnSubmitEvent OnSubmit;

        bool registered;

        void OnEnable()
        {
            if (!registered)
            {
                registered = true;
                PropertyValueInput.onEndEdit.AddListener(EndEditOnEnter);
            }
        }

        void OnDisable()
        {
            registered = false;
            PropertyValueInput.onEndEdit.RemoveListener(EndEditOnEnter);
        }

        public void SetValue(string value)
        {
            PropertyValueInput.text = value.ToString();
        }

        public void EndEditOnEnter(string value)
        {
            if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Tab))
            {
                this.SubmitForm(value.Trim());
            }
            else
            {
                this.SubmitForm(value);
            }
        }

        public void SubmitForm(string value)
        {
            OnSubmit.Invoke(value);
        }
    }
}
