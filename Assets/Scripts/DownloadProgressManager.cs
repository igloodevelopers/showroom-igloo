﻿using UnityEngine.UI;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

#pragma warning disable 618, 649
namespace Com.IglooVision.Showroom
{
    public class DownloadProgressManager : MonoBehaviourPunCallbacks
    {
        public Text currentDownloadItem;
        public GameObject downloadProgressLable;

        public void Start()
        {
            downloadProgressLable.SetActive(false);
            currentDownloadItem.text = "";
        }

        #region PUN CALLBACKS

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
        }

        private void Update()
        {
            // If there is progress happening
            if (SimpleBundleManager.currentDownloadProgress < 99.5f)
            {
                // if the progress label is not on, turn it on.
                if (!downloadProgressLable.activeInHierarchy) downloadProgressLable.SetActive(true);
                currentDownloadItem.text = $"Igloo Package : {(int)SimpleBundleManager.currentDownloadProgress}%";
            }
            else
            {
                // if the progress label if on, turn it off. After setting the download text to 0;
                currentDownloadItem.text = "";
                if (downloadProgressLable.activeInHierarchy) downloadProgressLable.SetActive(false);
            }
        }

        #endregion
    }
}
