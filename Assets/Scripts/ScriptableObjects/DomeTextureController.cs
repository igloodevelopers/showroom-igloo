﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

public class DomeTextureController : MonoBehaviour
{
    /// Serialized private variables 
    [Tooltip("Current material on outside of igloos")]
    [SerializeField] private Material currentIglooMaterial;
    [Tooltip("Current material applied to cylinder outers")]
    [SerializeField] private Material[] currentCylinderMaterials;        
    [Space]      
    [Tooltip("Name of subfolder in StreamingAssets where textures are stored")]
    [SerializeField] private string iglooTextureFolderName;
    [Tooltip("Highlight colour of the current active igloo texture")]
    [SerializeField] private Color CurrentTextureHightlight = Color.yellow;
    [Tooltip("Default texture applied to igloos")]
    [SerializeField] private Texture2D defaultIglooTexture;         
    [Space]      
    [Tooltip("Prefab for the texture label")]
    [SerializeField] private GameObject TextureNameLabel;                   
    [Tooltip("Menu where the content is stored")]
    [SerializeField] private GameObject TextureLabelContentHolder;

    /// Private variables
    // Active dictionary key. relating to the current active texture
    private string currentTextureKey;   
    // Entire system filepath, including subfolder
    private string filePath; 
    // List of texture labels for when changing colour of active label.
    private List<GameObject> TextureLabels = new List<GameObject>();
    // Iterator through list
    private int TextureIterator = 0;

    /// Public Variables
    // Dictionary to hold all igloo textures, and their key which will be the file name of the texture.
    public SortedDictionary<string, Texture2D> iglooTextures = new SortedDictionary<string, Texture2D>();

    /// <summary>
    /// Setting up scene values, and variables. 
    /// </summary>
    internal void Start()
    {
        // set file path
        filePath = Path.Combine(Application.streamingAssetsPath, iglooTextureFolderName);
        // initiate first grab of textures
        GetAllIglooTextures();
    }

    /// <summary>
    /// This searches through the StreamingAssets folder, and rebuilds the texture dictionary
    /// based on what it finds. 
    /// </summary>
    internal void GetAllIglooTextures()
    {
        // clear the dictionary to remove duplicates 
        iglooTextures.Clear();

        // Removes all texture labels
        TextureLabels.Clear();

        // Set up the file structure and directory information.
        DirectoryInfo directoryInfo = new DirectoryInfo(filePath);
        FileInfo[] allFiles = directoryInfo.GetFiles("*.*");
        print("Number of files found : " + allFiles.Length);

        // Load all the textures, ignoring the metas, then pull the Texture2D from them.
        foreach (var file in allFiles)
        {
            if (!file.Name.Contains(".meta"))
            {
                print("Loading Texture : " + file.Name);
                WWW www = new WWW(filePath + "/" + file.Name); // Get the file information
                Texture2D tex = www.texture; // Serialise the texture so operands can be applied to it, if neccessary.
                iglooTextures.Add(file.Name.Substring(0, file.Name.IndexOf(".")), tex); // add texture to the dictionary.
                CreateContentLabel(file.Name.Substring(0, file.Name.IndexOf(".")));
            }
        }

        iglooTextures.Add("default", defaultIglooTexture);
        currentTextureKey = "default";
        UpdateAllMaterials();
        print("Loaded cylinder textures : " + iglooTextures.Count);
    }


    /// <summary>
    /// This recieves a texture and applies it to both the igloo and cylinder materials
    /// It then sets the current texture key to this material so it is hightlighted in the GUI texture list.
    /// </summary>
    /// <param name="newTexture"></param>
    internal void UpdateAllMaterials()
    {
        // Set Cylinder texture
        for(int i = 0; i < currentCylinderMaterials.Length; i++)
        {
            currentCylinderMaterials[i].mainTexture = iglooTextures[currentTextureKey];
        }

        // Set Igloo Colour
        currentIglooMaterial.color = AverageColorFromTexture(iglooTextures[currentTextureKey]);
    }

    /// <summary>
    /// Retrives the average colour from a texture and returns it.
    /// </summary>
    /// <param name="tex"></param>
    /// <returns></returns>
    internal Color32 AverageColorFromTexture(Texture2D tex)
    {
        if (tex == null || currentTextureKey == "default") return Color.white;
        Color32[] texColors = tex.GetPixels32();

        int total = texColors.Length;

        float r = 0;
        float g = 0;
        float b = 0;

        for (int i = 0; i < total; i++)
        {

            r += texColors[i].r;

            g += texColors[i].g;

            b += texColors[i].b;

        }
        return new Color32((byte)(r / total), (byte)(g / total), (byte)(b / total), 0);
    }

    /// <summary>
    /// This is called by a button in the GUI
    /// It will iterate either forwards or backwards to the next texture in the list.
    /// </summary>
    /// <param name="isNext"></param>
    public void IterateTexture(bool isNext /* false isPrevious */)
    {
        // Look through the sorted dictionary, and get the next item. 
        string nextKey = "";
        if (isNext)
        {
            TextureIterator = (TextureIterator + 1) % (iglooTextures.Count);
            nextKey = iglooTextures.ElementAt(TextureIterator).Key;
        }
        else
        {
            TextureIterator = (TextureIterator - 1) % (iglooTextures.Count);
            nextKey = iglooTextures.ElementAt(TextureIterator).Key;
        }
        // Set the new key as the currentKey and then update the material and set the next colour label.
        currentTextureKey = nextKey;
        UpdateAllMaterials();
        ColourCurrentTextureLabel();
    }

    /// <summary>
    /// Sets the current label, which indicates which texture is currently active
    /// to the colour set at <param name="CurrentTextureHightlight"></param>
    /// </summary>
    public void ColourCurrentTextureLabel()
    {
        for (int i = 0; i < TextureLabels.Count; i++)
        {
            if (TextureLabels[i].name == currentTextureKey)
            {
                TextureLabels[i].GetComponent<Text>().color = CurrentTextureHightlight;
            }
            else
            {
                TextureLabels[i].GetComponent<Text>().color = Color.white;
            }
        }
    }

    private void CreateContentLabel(string labelName)
    {
        GameObject label = Instantiate(TextureNameLabel, TextureLabelContentHolder.transform, false) as GameObject;
        TextureLabels.Add(label);
        label.GetComponent<Text>().text = labelName;
        label.name = labelName;
    }


    ////////////////////// TESTING UPDATE FUNCTION ///////////////////////////
    internal void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            IterateTexture(true);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            IterateTexture(false);
        }
    }
}
