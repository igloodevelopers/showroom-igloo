﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    #region Inspector Variables
    public enum MenuLocation
    {
        Home, Settings, Furniture, Helpers, Covers, Null
    };
    private MenuLocation currentMenu = MenuLocation.Home;
    private bool bIsHomeOpen = false;
    public static string currentHighlightButton = "";
    [Serializable]
    public struct MenuPage
    {
        public MenuLocation location;
        public GameObject parent; 
    }
    public MenuPage[] menuPagesArr;
    public bool bDebug = true;
    private Dictionary<MenuLocation, GameObject> menuPages = new Dictionary<MenuLocation, GameObject>();
    #endregion

    private void Start()
    {
        FindAllVariables();
    }

    private void FindAllVariables()
    {
        for (int i = 0; i < menuPagesArr.Length; i++)
        {
            menuPages.Add(menuPagesArr[i].location, menuPagesArr[i].parent);
            menuPagesArr[i].parent.SetActive(false); // turn everything off at the start
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleHomeMenu(true);
        }
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            ToggleHomeMenu(false);
        }
    }

    /// <summary>
    /// Opens and closes the home menu based on the current inventory position
    /// </summary>
    /// <param name="bShouldOpen"></param>
    private void ToggleHomeMenu(bool bShouldOpen)
    {
        // if it is not currently open, but should open
        if(!bIsHomeOpen && bShouldOpen)
        {
            UpdateMenu(MenuLocation.Home); // Open the home menu
            bIsHomeOpen = true;            // It is now open
        }
        else if(bIsHomeOpen && !bShouldOpen && currentHighlightButton == "") // if home is open, and should close
        {
            UpdateMenu(MenuLocation.Null); // Open nothing (close everything)
            bIsHomeOpen = false;
        }
        else if (bIsHomeOpen && !bShouldOpen && currentHighlightButton != "") // if home is open, and should close
        {
            MoveToMenu(currentHighlightButton);
            bIsHomeOpen = false;
        }
        else
        {
            if (bDebug) Debug.Log("<b>[UI Controller]</b> Doing nothing as home is not open when right mouse released");
        }
    }

    public void MoveToMenu(string newMenuName)
    {
        switch (newMenuName)
        {
            case "Home":
                UpdateMenu(MenuLocation.Home);
                bIsHomeOpen = true;
                break;
            case "Covers":
                UpdateMenu(MenuLocation.Covers);
                bIsHomeOpen = false;
                break;
            case "Furniture":
                UpdateMenu(MenuLocation.Furniture);
                bIsHomeOpen = false;
                break;
            case "Helpers":
                UpdateMenu(MenuLocation.Helpers);
                bIsHomeOpen = false;
                break;
            case "Settings":
                UpdateMenu(MenuLocation.Settings);
                bIsHomeOpen = false;
                break;
            default:
                if(bDebug) Debug.LogWarning($"<b>[UI Controller]</b> Ambiguous string : {newMenuName} returns null");
                UpdateMenu(MenuLocation.Null);
                bIsHomeOpen = false;
                break;
        }
    }

    /// <summary>
    /// Opens the new menu, and closes any active one. 
    /// </summary>
    /// <param name="newLoc"></param>
    internal void UpdateMenu(MenuLocation newLoc)
    {
        // Close current menu location, if it is not Null
        if(currentMenu != MenuLocation.Null)
        {
            if (menuPages.TryGetValue(currentMenu, out GameObject curMenu))
            {
                curMenu.SetActive(false);
            }
            else Debug.LogError($"<b>[UI Controller]</b> Failed to retreive {currentMenu.ToString()}");
        }

        // Open new menu, unless it is Null
        if(newLoc != MenuLocation.Null)
        {
            if (menuPages.TryGetValue(newLoc, out GameObject newMenu))
            {
                newMenu.SetActive(true);
                currentMenu = newLoc;
            }
            else Debug.LogError($"<b>[UI Controller]</b> Failed to retreive {newLoc.ToString()}");
        }
    }
}
