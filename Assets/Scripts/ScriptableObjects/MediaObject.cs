﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediaObject : MonoBehaviour {

    public enum MediaType { Null, AVProVideo };

    protected Material mediaMaterial;
    public virtual Material MediaMaterial { set { mediaMaterial = value; } }

    private bool isLoaded;
    private bool canPlay;
    public virtual bool IsLoaded { get { return isLoaded; } set { isLoaded = value; } }
    public virtual bool CanPlay { get { return canPlay; } set { canPlay = value; } }

    public virtual int GetMediaWidth() { return -1; }
    public virtual int GetMediaHeight() { return -1; }
    public virtual float GetMediaPos() { return -1; } // Should be normalised Value

    public virtual void Start()
    {

    }

    public virtual void Play()
    {

    }

    public virtual void Pause()
    {

    }

    public virtual void SetPause(int value)
    {

    }

    public virtual bool IsPaused()
    {
        return false;
    }

    public virtual void Stop()
    {

    }

    public virtual void SetPosition(float value)
    {

    }

    public virtual void SetVolume(float value)
    {

    }

    public virtual float GetPosition()
    {
        float position = 0;
        return position;
    }

    public virtual bool Load(string path, bool isAutoPlay)
    {
        return false;
    }
}
