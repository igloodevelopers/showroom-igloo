﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnObjects : MonoBehaviour
{
    #region Inspector Variables
    [TooltipAttribute("The tile GameObject that make up the grid")]
    [SerializeField] GameObject productionTile;

    [TooltipAttribute("The layer in which the terrain is placed")]
    [SerializeField] LayerMask terrainLayer;

    [TooltipAttribute("Need GraphicRaycaster to detect click on a button")]
    [SerializeField] GraphicRaycaster uiRaycaster;

    [Tooltip("Parent object for all objects that are spawned")]
    [SerializeField] GameObject ObjectParent;

    [SerializeField] BuildProgressSO buildingToPlace;
    #endregion

    #region Instance Objects
    GameObject currentSpawnedObject;
    RaycastHit hit;
    List<ProductionTile> activeTiles;
    GameObject activeTilesParent;
    #endregion

    void Start()
    {
        activeTiles = new List<ProductionTile>();
        if (!productionTile)
            Debug.LogError("Production Tile is NULL");
        if (!uiRaycaster)
            Debug.Log("GraphicRaycaster not found! Will place objects on button click");
    }


    void Update()
    {
        if (currentSpawnedObject)
        {
            if (Input.GetKey(KeyCode.R))
            {
                currentSpawnedObject.transform.Rotate(new Vector3(0,0, 1f));
            }
            if (Input.GetMouseButtonDown(0))
            {
                if (!PlacementHelpers.RaycastFromMouse(out hit, terrainLayer)) 
                {
                    Debug.Log($"No hit recorded: {hit.collider.gameObject.name}");
                    return;
                }
                    

                currentSpawnedObject.transform.position = hit.point;

                if (CanPlaceBuilding())
                    PlaceBuilding();
                else
                {
                    Debug.Log($"Cannot place here : {hit.collider.gameObject.name}");
                }
            }
            if (Input.GetMouseButtonDown(1))
                Destroy(currentSpawnedObject);
        }
    }


    void FixedUpdate()
    {
        if (currentSpawnedObject)
            if (PlacementHelpers.RaycastFromMouse(out hit, terrainLayer))
                currentSpawnedObject.transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
    }


    bool CanPlaceBuilding()
    {
        if (PlacementHelpers.IsButtonPressed(uiRaycaster))
        {
            Debug.Log($"UI Raycaster button pressed");
            return false;
        }
            
        for (int i = 0; i < activeTiles.Count; i++)
            if (activeTiles[i].colliding)
            {
                Debug.Log($"Active Tile collision with : {activeTiles[i].name}");
                return false;
            }
                
        return true;
    }


    void PlaceBuilding()
    {
        ClearGrid();
        StartCoroutine(BeginBuilding());
    }


    void ClearGrid()
    {
        Destroy(activeTilesParent);
        activeTiles.RemoveAll(i => i);
    }


    IEnumerator BeginBuilding()
    {
        Vector3 pos = currentSpawnedObject.transform.position;
        GameObject instance = currentSpawnedObject;
        currentSpawnedObject = null;

        if (PlacementHelpers.RaycastFromMouse(out RaycastHit hitTerrain, terrainLayer))
            pos = hitTerrain.point;
        
        PlacementHelpers.ToggleRenderers(instance, true);
        instance.transform.SetParent(ObjectParent?.transform);
        Debug.Log("instance parent: " + instance.transform.parent.name);
        yield return null;
    }


    void FillRectWithTiles(Collider col)
    {
        if (activeTilesParent)
            return;

        Rect rect = PlacementHelpers.MakeRectOfCollider(col);
        float fromX = rect.position.x;
        float toX = (rect.position.x + rect.width) * col.gameObject.transform.localScale.x;
        float fromZ = rect.position.y;
        float toZ = (rect.position.y + rect.height) * col.gameObject.transform.localScale.z;

        GameObject parent = new GameObject("PlacementGrid");
        parent.transform.SetParent(col.gameObject.transform.root);
        parent.transform.position = col.gameObject.transform.InverseTransformPoint(new Vector3(0, 0.5f, 0));

        for (float i = -toX / 2; i <= toX / 2; i += productionTile.transform.localScale.x)
        {
            for (float j = -toZ / 2; j <= toZ / 2; j += productionTile.transform.localScale.y)
            {
                GameObject tile = Instantiate(productionTile);
                tile.transform.SetParent(parent.transform);
                tile.transform.position = new Vector3(i, parent.transform.position.y, j);
                activeTiles.Add(tile.GetComponent<ProductionTile>());
            }
        }
        activeTilesParent = parent;
    }


    public void SpawnObject(SpawnableObject obj)
    {
        // if haven't placed the spawned building, then return
        if (currentSpawnedObject)
            return;

        currentSpawnedObject = Instantiate(obj.ObjectPrefab, ObjectParent?.transform);
        buildingToPlace.currentBuilding = obj;
        // PlacementHelpers.ToggleRenderers(currentSpawnedObject, false);
        Collider[] cols = currentSpawnedObject.GetComponentsInChildren<Collider>();
        //if (cols.Length > 0)
        //    FillRectWithTiles(cols[0]);
        //else
        //    Debug.LogError("Building has no colliders");
    }
}
