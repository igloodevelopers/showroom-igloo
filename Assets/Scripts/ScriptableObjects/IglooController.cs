﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using Photon.Realtime;
using Photon.Pun;
using ExitGames.Client.Photon;

namespace Com.IglooVision.Showroom
{
    public class IglooController : MonoBehaviour, IOnEventCallback
    {
        const string scenePrefix = "2018_Master_";
        public string startScene;
        [SerializeField] GameObject placedObjectsParent = null;
        public Igloo currentIgloo { get; private set; }
        List<string> allSceneNames = new List<string>(); // stores a list of active scenes 
        public const byte LoadNewIglooSceneFromAssetBundle = 1;
        bool hasStarted = false;

        [System.Serializable]
        public struct IglooButtons
        {
            public string IglooName;
            public Button IglooButton;
            public Transform parent;
        }
        public IglooButtons[] iglooButtons;

        [Header("Special arrangements")]
        [SerializeField] GameObject boardRoom;

        /**
         The start function is used to check through all the buttons and disable any that are not in the scene
         selection. This helps later as when we add more buttons, we do not have to go through each scene and update
         their availiabilty. It should do it automatically when the scene is added to the build list. 
         
         First, however, we should check that the current active scene is also 
         disabled to avoid re-loading the active scene.
        */
        public void Awake()
        {
            //SimpleBundleManager.LoadLevelAssetBundle("igloo_materials");
            if (PhotonNetwork.IsConnectedAndReady && PhotonNetwork.IsMasterClient)
            {
                // Load first igloo
                //LoadIgloo(startScene);
            }
        }

        public void OnEnable()
        {
            PhotonNetwork.AddCallbackTarget(this);
        }

        private void OnDisable()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        public void LoadIglooScenesBundle(int iglooIndex)
        {
            if (!hasStarted)
            {
                hasStarted = true;
            }
            object[] content = new object[] { iglooIndex };
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(LoadNewIglooSceneFromAssetBundle, content, raiseEventOptions, SendOptions.SendReliable);
        }
        #region PUN CALLBACKS

        public void OnEvent(EventData photonEvent)
        {
            
            if (photonEvent.Code == LoadNewIglooSceneFromAssetBundle)
            {
                object[] data = (object[])photonEvent.CustomData;
                Debug.Log($"IglooController::OnEvent - Received data IglooIndex {(int)data[0]}");
                LoadIgloo((int)data[0]);
            }
        }

        #endregion

        private void FinishedLoadingIgloo(GameObject newIgloo)
        {

            currentIgloo = new Igloo
            {
                iglooName = newIgloo.gameObject.name,
                iglooObj = newIgloo
            };
            SetIglooButtons();
        }

        private void SetIglooButtons()
        {
            Debug.Log($"IglooController::SetIglooButtons - Current Igloo Name {currentIgloo.iglooName}");
            foreach (IglooButtons sys in iglooButtons)
            {
                if (sys.IglooName == currentIgloo.iglooName)
                {
                    sys.IglooButton.GetComponentInParent<Selection_System>().thisIglooIsSelected = true;
                }
                else
                {
                    sys.IglooButton.GetComponentInParent<Selection_System>().thisIglooIsSelected = false;
                }
            }
        }

        /// <summary>
        /// Loads a scene based on the string provided. 
        /// Checks the string against the current active scene, to make sure we don't load the scene twice
        /// 
        /// </summary>
        /// <param name="iglooIndex"></param>
        public void LoadIgloo(int iglooIndex)
        {
            Debug.Log("IglooController::LoadIgloo - Igloo to load : " + iglooIndex);
            DestroyAllPlacedObjects(); // Remove anything in the scene that may have been placed around. TODO make this an option in Settings

            SimpleBundleManager.RequestLoadIgloo(iglooIndex);

            MakeSpecialArrangements();

            foreach (IglooButtons btn in iglooButtons)
            {
                // if the current scene is active disable the button for that scene.
                // also if the list of scenes does not contain that scene (its not avaliable to load) then disable that button.
                //if ((scenePrefix + btn.IglooName) == currentIgloo.name || !allSceneNames.Contains(scenePrefix + btn.IglooName))
                //{
                //    btn.IglooButton.interactable = false;
                //}
                //else
                //{
                //    btn.IglooButton.interactable = true;
                //}
            }
        }

        private void DestroyAllPlacedObjects()
        {
            Transform[] objs = placedObjectsParent.GetComponentsInChildren<Transform>();
            foreach (Transform obj in objs)
            {
                if (obj.name != placedObjectsParent.name)
                    Destroy(obj.gameObject);
            }
        }

        private void MakeSpecialArrangements()
        {
            //switch (CurrentActiveIglooScene.name)
            //{
            //    case scenePrefix + "Meeting_Room":
            //        if (boardRoom == null) return;
            //        GameObject BR = Instantiate(boardRoom, placedObjectsParent?.transform);
            //        BR.transform.localPosition = new Vector3(.566f, .38f, .38f);
            //        BR.transform.localRotation = Quaternion.Euler(new Vector3(-90f, 180, 91.32f));
            //        break;
            //}
        }
    }

    public class Igloo
    {
        public string iglooName;
        public GameObject iglooObj;
    }

}
