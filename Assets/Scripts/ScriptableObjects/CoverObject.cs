﻿using UnityEngine;

[CreateAssetMenu(fileName = "Cover", menuName = "ScriptableObject/Cover", order = 1)]
public class CoverObject : ScriptableObject
{
    public string coverName = "Object Name";
    public Texture coverTexture = null;
    public Sprite icon = null;
}
