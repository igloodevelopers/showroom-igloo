﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System;

namespace AnyVision
{
    public class MediaXMLReader : MonoBehaviour
    {
        private static MediaXMLReader instance;
        public static MediaXMLReader Instance
        {
            get
            {
                if (instance == null) instance = FindObjectOfType<MediaXMLReader>();
                return instance;
            }
        }
        public MediaSettings mediaSettings;
        public string dataPath = "";

        public delegate void LoadAction();
        public static event LoadAction OnLoad;

        public delegate void SaveAction();
        public static event SaveAction OnSave;

        public bool GetMediaInformation()
        { 
            if (dataPath != "")
            {
                try
                {
                    LoadMediaData(Path.Combine(Application.dataPath, dataPath));
                }
                catch (Exception e)
                {
                    Debug.LogError(e.ToString());
                }
                return true;
            }
            else
            {
                Debug.LogWarning("GameData is not entered correctly.");
                return false;
            }
        }

        public bool SaveMediaInformation()
        {
            if (dataPath != "")
            {
                try
                {
                    SaveMediaData(Path.Combine(Application.dataPath, dataPath));
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.ToString());
                }
                return true;
            }
            else
            {
                Debug.LogWarning("Datapath is not entered correctly.");
                return false;
            }
        }

        public void SaveMediaData(string path)
        {
            if (OnSave != null) OnSave();

            XmlSerializer serializer = new XmlSerializer(typeof(MediaSettings));
            Debug.Log("Saving to path :  " + path);
            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, mediaSettings);
            }
        }

        public bool LoadMediaData(string path)
        {
            Debug.Log(path);
            XmlSerializer serializer = new XmlSerializer(typeof(MediaSettings));
            MediaSettings save;
            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                save = (MediaSettings)serializer.Deserialize(stream);
            }
            mediaSettings = save;

            if (OnLoad != null) OnLoad();

            return true;
        }

        [XmlRoot(ElementName = "MediaSettings")]
        public class MediaSettings
        {
            [XmlElement(ElementName = "mediaSetting")]
            public List<mediaSetting> mediaSettings { get; set; }
        }

        [XmlRoot(ElementName = "mediaSetting")]
        public class mediaSetting
        {
            [XmlAttribute(AttributeName = "MediaName")]
            public string MediaName { get; set; }
            [XmlAttribute(AttributeName = "ScaleX")]
            public float ScaleX { get; set; }
            [XmlAttribute(AttributeName = "ScaleY")]
            public float ScaleY { get; set; }
            [XmlAttribute(AttributeName = "OffsetX")]
            public float OffsetX { get; set; }
            [XmlAttribute(AttributeName = "OffsetY")]
            public float OffsetY { get; set; }
        }

    }
}

