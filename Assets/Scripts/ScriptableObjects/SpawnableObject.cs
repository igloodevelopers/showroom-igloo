﻿using UnityEngine;

[CreateAssetMenu(fileName = "Spawnable", menuName = "ScriptableObject/Spawnable", order = 1)]
public class SpawnableObject : ScriptableObject
{
    public string objectName = "Object Name";
    public GameObject ObjectPrefab = null;
    public Sprite icon = null;
}
