﻿using UnityEngine.UI;
using UnityEngine;

public class Settings : MonoBehaviour
{
    [SerializeField] private Text wrapAdjustmentText = null;
    [SerializeField] private Text fovAdjustmentText = null;
    [SerializeField] private Text coverWrapAdjustmentText = null;
    [SerializeField] private Text horizontalAdjustmentText = null;
    [SerializeField] private Material iglooMovieMat = null;
    [SerializeField] private Material coverMaterial = null;

    private void Start()
    {
        AdjustWrapPosition(0);
        AdjustFOV(60);
        AdjustCoverPosition(0);
        AdjustHorizontalPosition(1);
    }

    public void AdjustWrapPosition(float val)
    {
        iglooMovieMat.mainTextureOffset = new Vector2(val, 0);
        wrapAdjustmentText.text = $"Wrap Adjustment : {string.Format("{0:0.0#}", val).ToString()}";
    }

    public void AdjustFOV(float val)
    {
        val = Mathf.Clamp((int)val, 1, 179);
        Camera.main.fieldOfView = val;
        fovAdjustmentText.text = $"Field of View : {val}";
    }

    public void AdjustCoverPosition(float val)
    {
        coverMaterial.mainTextureOffset = new Vector2(val, 0);
        coverWrapAdjustmentText.text = $"Cover Adjustment : {string.Format("{0:0.0#}", val).ToString()}";
    }

    public void AdjustHorizontalPosition(float val)
    {
        iglooMovieMat.mainTextureScale = new Vector2(1, val);
        horizontalAdjustmentText.text = $"Horizontal Adjustment : {string.Format("{0:0.0#}", val).ToString()}";
    }
}
