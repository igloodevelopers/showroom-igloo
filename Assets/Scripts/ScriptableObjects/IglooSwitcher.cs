﻿using UnityEngine;
using UnityEngine.UI;

public class IglooSwitcher : MonoBehaviour
{
    [System.Serializable]
    public struct Igloos
    {
        public string name;
        public GameObject igloo;
        public Texture2D iglooPlaque;
        public Vector3 podiumSize;
    }
    public Igloos[] igloos;
    
    private int nextIglooExpression = 1;
    private int currentIglooID = 0;
    private bool individualSelection = false;
    private int TestingTesting = 0;
    private int iglooIDToOpen = 0;

    public Button[] consoleButtons;
    public Material plaqueMat;


    private bool rotationLock = false;
    private Vector3 origRotation;

#region System and Locking

    /// <summary>
    /// TESTING ONLY!! + commented ones are not in the system yet
    /// </summary>
    public virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow)) ExitDome();
        if (Input.GetKeyDown(KeyCode.UpArrow)) EnterDome();
        if (Input.GetKeyDown(KeyCode.Alpha1)) SelectDome(1); // 6m Cylidnder
        if (Input.GetKeyDown(KeyCode.Alpha2)) SelectDome(0); // 6m Dome
        if (Input.GetKeyDown(KeyCode.Alpha4)) SelectDome(2); // 9m Cylinder
        if (Input.GetKeyDown(KeyCode.Alpha5)) SelectDome(3); // 9m Dome
        if (Input.GetKeyDown(KeyCode.Alpha6)) SelectDome(4); // 12m Dome

    }

    public void LockConsoleControls()
    {
        for(int i = 0; i < consoleButtons.Length; i++)
        {
            consoleButtons[i].enabled = false;
        }

    }

    public void UnlockConsoleControls()
    {
        for (int i = 0; i < consoleButtons.Length; i++)
        {
            consoleButtons[i].enabled = true;
        }
    }

#endregion

    #region Interaction from Control Pad
    /// <summary>
    /// Opens the door, then fade out and appear inside the dome. 
    /// Switch control systems inner dome controls
    /// occlude anything outside the dome
    /// play any spout movies. 
    /// </summary>
    public void EnterDome()
    {
        LockConsoleControls();
    }

    /// <summary>
    /// Opens the door, then fade out and appear outside the dome.
    /// Switch Control Systems
    /// Turn off Spout *for optimisation.
    /// </summary>
    public void ExitDome()
    {
        LockConsoleControls();
    }

    /// <summary>
    /// Initiate animation and move the next dome into the scene.
    /// The current dome in the scene will move to the right, whilst a new dome moves in to the center from the left
    /// It will be accompanied by a sick animation where it fades out to wireframe, then invisible. 
    /// </summary>
    public void NextDome()
    {
        LockConsoleControls();
        rotationLock = true;
        nextIglooExpression = 1;
    }


    /// <summary>
    /// This is the same as the NextDome() function, but in reverse. 
    /// </summary>
    public void PreviousDome()
    {
        LockConsoleControls();
        rotationLock = true;
        nextIglooExpression = -1;
    }

    /// <summary>
    /// This function does the same as the above, but causes the next process to switch to a specific dome drawn,
    /// This is caused by pressing either a number key, or a specific dome button within the scene. 
    /// </summary>
    /// <param name="iglooIDToOpen"></param>
    public void SelectDome(int newIglooID)
    {
        individualSelection = true;
        iglooIDToOpen = newIglooID;
        Debug.Log("IglooIDToOpen : " + iglooIDToOpen);
        LockConsoleControls();
        rotationLock = true;
    }

    public void EnableAllOtherButtons(Button currentButton)
    {
        for(int i = 0; i < consoleButtons.Length; i++)
        {
            if (currentButton == consoleButtons[i]) break;
            else
            {
                consoleButtons[i].interactable = true;
            }
        }
    }

    public void DomeTransitionComplete()
    {
        UnlockConsoleControls();
    }

    #endregion

}
