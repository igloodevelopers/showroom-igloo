﻿using Photon.Pun;
using UnityEngine;
using Photon.Compression;
using Photon.Pun.Simple;

/// The PackObject attribute tells the code gen engine and the NetObject that this class/struct
/// contains [SyncVar] attributes that should be serialized and synced.
[PackObject]
[RequireComponent(typeof(NetObject))]
public class ByteStreamSenderPhoton : MonoBehaviour
{
    [HideInInspector, SyncVar(applyCallback = "OnApply", snapshotCallback = "OnSnapshot", interpolate = false, keyRate = KeyRate.Every)]
    public byte[] ByteStreamArray;

    /// <summary>
    /// Be invoked when received bytes
    /// </summary>
    public UnityEventByteArray OnReceivedByteDataEvent;

    private void Start()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            GetComponent<GameViewDecoder>().enabled = false;
        }
        else
        {
            GetComponent<GameViewEncoder>().enabled = false;
        }
    }

    public void SendNewByteArray(byte[] byteArr)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            ByteStreamArray = byteArr;
        }
    }

    /// applyCallback fires remotely when the synced value has changed.
    public void OnApply(byte[] newValue, byte[] oldValue)
    {
        /// If this is a client, not the server. (receiver, not sender) 
        if (!PhotonNetwork.IsMasterClient)
        {
            OnReceivedByteDataEvent.Invoke(newValue);
        }
    }


    /// snapshotCallback fires remotely every simulation tick, whether or not the value changed.
    public void OnSnapshot(byte[] snap, byte[] targ)
    {
        /// snapshotCallback gets called every tick, whether the value has changed or not.
    }
}
